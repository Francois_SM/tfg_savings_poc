﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts {
    public class ThoughtCard : ScriptableObject {
        public string Text;
        public Color CardColor;
        [TextArea]
        public string Description;

        [Header("Option 1")]
        public string OptionDescription1;
        [TextArea]
        public string ResultDescription1;
        public int CostImmediate1 = 0;
        public int CostWeekly1 = 0;
        public int SatisfactionEffect1 = 0;

        [Header("Option 2")]
        public string OptionDescription2;
        [TextArea]
        public string ResultDescription2;
        public int CostImmediate2 = 0;
        public int CostWeekly2 = 0;
        public int SatisfactionEffect2 = 0;

        [Header("Option 3")]
        public string OptionDescription3;
        [TextArea]
        public string ResultDescription3;
        public int CostImmediate3 = 0;
        public int CostWeekly3 = 0;
        public int SatisfactionEffect3 = 0;
        
    }
}
