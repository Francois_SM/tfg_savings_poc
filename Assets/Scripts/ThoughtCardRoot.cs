﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class ThoughtCardRoot : MonoBehaviour {

	public string Text;
	public Color CardColor;
	public string Description;

	[Header("Option 1")]
	public string OptionDescription1;
	public string ResultDescription1;
	public int CostImmediate1 = 0;
	public int CostWeekly1 = 0;
	public int SatisfactionEffect1 = 0;

	[Header("Option 2")]
	public string OptionDescription2;
	public string ResultDescription2;
	public int CostImmediate2 = 0;
	public int CostWeekly2 = 0;
	public int SatisfactionEffect2 = 0;

	[Header("Option 3")]
	public string OptionDescription3;
	public string ResultDescription3;
	public int CostImmediate3 = 0;
	public int CostWeekly3 = 0;
	public int SatisfactionEffect3 = 0;

	private GameController _gameController;

	// Use this for initialization
	void Start () {
		_gameController = GameObject.Find("GameController").GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
