﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class ThoughtCardController : MonoBehaviour {

	public ThoughtCardRoot CardInfo;

	private Text _text;
	private Image _panel;
	private ScrollingScript _scrollController;
	private GameController _gameController;

	// Use this for initialization
	void Start () {

		_panel = gameObject.GetComponent<Image>();
		_text = gameObject.transform.GetChild(0).gameObject.GetComponent<Text>();
		_scrollController = GameObject.Find("ScrollingPanel").GetComponent<ScrollingScript>();
		_gameController = GameObject.Find("GameController").GetComponent<GameController>();

		//SetColor();
		SetText();
	}

	// Update is called once per frame
	void Update () {
		if (IsOffLeftSide()) {
			_scrollController.MoveCardToBack();
		}
	}

	public void SetColor() {
		//_panel.color = CardInfo.CardColor;
	}

	void SetText() {
		if (CardInfo.Text.Trim().Length > 0) {
			_text.text = CardInfo.Text.Trim();
		}
	}

	public void OnClick() {
		_scrollController.StopScrolling();
		_gameController.ShowChoicePanel(CardInfo);

	}

	public bool IsOffLeftSide() {
		return (gameObject.transform.position.x + (gameObject.GetComponent<RectTransform>().sizeDelta.x / 2) < 0);
	}
}
