﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class ScrollingScript : MonoBehaviour {

	public float scrollPercentPerSecond = 0.2f;

	private Transform _content;
	private Vector2 _resolution;
	private GameController _gameController;
	private bool _scrolling = true;

	// Use this for initialization
	void Start () {
		_content = GameObject.Find("Content").transform;
		_resolution = (GameObject.Find("Canvas")).GetComponent<CanvasScaler>().referenceResolution;
		_gameController = GameObject.Find("GameController").GetComponent<GameController>();
	}

	// Update is called once per frame
	void Update() {
		if (_scrolling) {
			if (_content != null) {
				float addedX = scrollPercentPerSecond * Time.deltaTime * _resolution.x;
				_content.position -= new Vector3(addedX, 0f, 0f);
			}
		}
	}

	public void MoveCardToBack() {
		Debug.Log("Moving card to back.");
		GameObject leftmostCard = _content.GetChild(0).gameObject,
			rightmostCard = _content.GetChild(0).gameObject;
		foreach (Transform card in _content.transform) {
			if (card.position.x <= leftmostCard.transform.position.x)
				leftmostCard = card.gameObject;
			if (card.position.x >= rightmostCard.transform.position.x)
				rightmostCard = card.gameObject;
		}
		leftmostCard.transform.position =
			rightmostCard.transform.position + new Vector3(rightmostCard.GetComponent<RectTransform>().sizeDelta.x*1.1f, 0f, 0f);
	}

	public void StartScrolling() {
		_scrolling = true;
	}

	public void StopScrolling() {
		_scrolling = false;
	}
}
