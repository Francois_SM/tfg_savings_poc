﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Minigame_Sorting {
	public class MinigameLetterSortController : MonoBehaviour {

		private List<Color> _redColors;
		private List<Color> _blueColors;

		public GameObject LetterSpawningObject;
		public GameObject LetterPrefab;

		public int LettersToSpawn = 10;
		[SerializeField] private int _xSpawnDeviation = 400;
		[SerializeField] private int _ySpawnDeviation = 200;
		[SerializeField] private int _rotationSpawnDeviation = 45;

		private GameController _gameController;

		public enum LetterColorGroup {
			Red,
			Blue
		};

		// Use this for initialization
		void Start () {
			_gameController = GameObject.Find("GameController").GetComponent<GameController>();
			
			SetupColors();
			SpawnLetters();
		}

		// Update is called once per frame
		void Update () {

		}

		private void SpawnLetters() {
			for (int i = 0; i < LettersToSpawn; i++) {
				var letter = Instantiate(LetterPrefab, LetterSpawningObject.transform);
				GetRandomPosition(letter);
				//ColorLetter(letter.GetComponent<LetterController>());
			}
		}

		public void GetRandomPosition(GameObject letter) {
			letter.transform.localPosition = new Vector3(
				Random.Range(-_xSpawnDeviation, _xSpawnDeviation), Random.Range(-_ySpawnDeviation, _ySpawnDeviation), 0f);
			letter.transform.rotation = Quaternion.Euler(0f, 0f, Random.Range(-_rotationSpawnDeviation, _rotationSpawnDeviation));
		}

		private void SetupColors() {
			_redColors = new List<Color>();
			_blueColors = new List<Color>();

			_redColors.Add(new Color(0.953125f, 0.359375f, 0.2578125f));
			_redColors.Add(new Color(0.85546875f, 0.20703125f, 0.09375f));
			_redColors.Add(new Color(0.734375f, 0.140625f, 0.140625f));
			_redColors.Add(new Color(1f, 0.61328125f, 0.546875f));

			_blueColors.Add(new Color(0.4375f, 0.46484375f, 1f));
			_blueColors.Add(new Color(0.23828125f, 0.23828125f, 0.796875f));
			_blueColors.Add(new Color(0.03125f, 0.03125f, 0.64453125f));
			_blueColors.Add(new Color(0.09375f, 0.09375f, 0.09375f));
		}

		public void ColorLetter(LetterController letter) {
			letter.Colorgroup = (Random.Range(0f, 1f) <= 0.5f ? LetterColorGroup.Red : LetterColorGroup.Blue);
			if (letter.Colorgroup == LetterColorGroup.Red) {
				letter.LetterColor = _redColors[Random.Range(0, _redColors.Count - 1)];
			}
			else{
				letter.LetterColor = _blueColors[Random.Range(0, _blueColors.Count - 1)];
			}
			letter.gameObject.GetComponent<Image>().color = letter.LetterColor;
		}

		public void ScoreLetter(LetterController letter) {
			Debug.Log("Correct! Scorring and recoloring letter");
			//letter = ColorLetter(letter);
			_gameController.AddPay();
			//Destroy(letter.gameObject);
			RespawnLetter(letter.gameObject);
			
		}

		void RespawnLetter(GameObject letter) {
			letter.transform.SetAsFirstSibling();
			ColorLetter(letter.GetComponent<LetterController>());
			GetRandomPosition(letter.gameObject);
			Vector3 newPos = letter.gameObject.transform.position;
			letter.transform.localPosition -= new Vector3(1080f, 0f, 0f);
			StartCoroutine(letter.GetComponent<LetterController>().SlideIn(newPos, 0.3f));
		}
	}
}
