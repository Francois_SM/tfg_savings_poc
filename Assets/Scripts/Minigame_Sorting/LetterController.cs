﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Minigame_Sorting {
	public class LetterController : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler{
		// Use this for initialization
		private Vector3 _originalPosition;

		public Color LetterColor;
		public MinigameLetterSortController.LetterColorGroup Colorgroup;
		private MinigameLetterSortController _minigameController;

		// Use this for initialization
		void Start () {
			this.transform.SetAsLastSibling();
			_minigameController = GameObject.Find("Minigame_Sort").GetComponent<MinigameLetterSortController>();
			_minigameController.ColorLetter(this);
			_originalPosition =
				new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);

			//PickColor();
		}
	
		// Update is called once per frame
		void Update () {
		
		}


		public void OnDrag(PointerEventData eventData) {
			gameObject.transform.SetAsLastSibling();
			gameObject.transform.position = eventData.position;
		}

		public void OnEndDrag(PointerEventData eventData) {
			GetComponent<CanvasGroup>().blocksRaycasts = true;
			gameObject.transform.position = _originalPosition;
		}

		public void OnBeginDrag(PointerEventData eventData) {
			GetComponent<CanvasGroup>().blocksRaycasts = false;
		}
		
		public IEnumerator SlideIn(Vector3 newPos, float time) {
			float elapsedTime = 0;
			Vector3 startingPos = transform.position;
			while (elapsedTime < time) {
				transform.position = Vector3.Lerp(startingPos, newPos, (elapsedTime / time));
				elapsedTime += Time.deltaTime;
				yield return null;;
			}

		}
	}
}
