﻿using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Minigame_Sorting {
	public class DropBoxController : MonoBehaviour, IDropHandler {

		public MinigameLetterSortController.LetterColorGroup BoxColor;
		private MinigameLetterSortController _minigameController;

		// Use this for initialization
		void Start () {
			_minigameController = GameObject.Find("Minigame_Sort").GetComponent<MinigameLetterSortController>();
		}
	
		// Update is called once per frame
		void Update () {
		
		}

		public void OnDrop(PointerEventData eventData) {
			Debug.Log("Dropped object: " + eventData.pointerDrag.name + ", Dropped in " + gameObject.name);
			var letter = eventData.pointerDrag.GetComponent<LetterController>();
			if (letter.Colorgroup == BoxColor) {
				_minigameController.ScoreLetter(letter);
			}
		}
	}
}
