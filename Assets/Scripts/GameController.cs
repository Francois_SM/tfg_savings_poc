﻿//using System.Diagnostics;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
	public class GameController : MonoBehaviour {

		public GameObject CardPrefab;
		public GameObject ScrollPanelObject;
		public GameObject ChoicePanelObject;
		public GameObject InfoPanelObject;
		public GameObject ResultsPanelObject;
		public GameObject BlockingPanelObject;

		private ThoughtCard[] _cards;
		private ScrollingScript _scrollController;
		private ThoughtCardRoot _currentChoice;
		private Slider _satisfactionSlider;
		private Image _timerGraphic;


		private int _cash = 0;
		private int _weeklyExpense = 0;
		private int _savings = 0;
		private int _investments = 0;
		private int _satisfaction = 50;

		[Header("Player parameters")]
		public int Pay = 4;
		public float WeekTimeInSec = 30f;
		public float WeeklyDissatisfaction = 10f;
		private bool _timerRunning = false;
		private float _weekTimer;
		private float _satisfactionTimer;

		// Use this for initialization
		void Start () {

			//Debug
			_cash = 1000;

			_scrollController = GameObject.Find("ScrollingPanel").GetComponent<ScrollingScript>();
			_satisfactionSlider = GameObject.Find("SatisfactionSlider").GetComponent<Slider>();
			_timerGraphic = GameObject.Find("ClockFill").GetComponent<Image>();

			LoadCards();
			AddCardsAndSpace();

			AddSatisfaction(0);

			_weekTimer = WeekTimeInSec;
			_satisfactionTimer = WeekTimeInSec;
			_timerRunning = true;
		}

		// Update is called once per frame
		void Update () {
			UpdateCashValues();
			UpdateTimer();
		}

		void UpdateTimer() {
			if (_timerRunning) {
				_weekTimer -= Time.deltaTime;
				if ((_satisfactionTimer - (WeekTimeInSec / WeeklyDissatisfaction)) > _weekTimer) {
					AddSatisfaction(-(int)(WeekTimeInSec / WeeklyDissatisfaction));
					_satisfactionTimer -= (WeekTimeInSec / WeeklyDissatisfaction);
				}
				if (_weekTimer <= 0) {
					_weekTimer = WeekTimeInSec;
					_satisfactionTimer = WeekTimeInSec;
					MakeWeeklyDeductions();
				}
				_timerGraphic.fillAmount = _weekTimer / WeekTimeInSec;
			}
		}

		void UpdateCashValues() {
			InfoPanelObject.transform.FindChild("CashText").GetComponent<Text>().text =
				"Cash: R" + _cash + ",   - R" + _weeklyExpense + "/wk";
		}

		void LoadCards() {
			_cards = Resources.LoadAll<ThoughtCard>("Cards/") as ThoughtCard[];
		}

		void AddCardsAndSpace() {
			for (int i = 0; i < _cards.Length; i++) {
				var card = _cards[i];
				var tempCard = Instantiate(CardPrefab, ScrollPanelObject.transform);
				CopyCardDetails(card, tempCard);
				Debug.Log("Card sizeDelta.x = " + tempCard.transform.GetComponent<RectTransform>().sizeDelta.x );
				Debug.Log("Original posX = "+ tempCard.transform.position.x);
				tempCard.transform.localPosition = new Vector3(
					(tempCard.transform.GetComponent<RectTransform>().sizeDelta.x *1.1f * i),
					(-tempCard.transform.GetComponent<RectTransform>().sizeDelta.y / 2 * 1.2f)
					,0);
				tempCard.GetComponent<Image>().color = card.CardColor;
			}
		}

		public ThoughtCard[] GetCards() {
			return _cards;
		}

		public void ShowChoicePanel(ThoughtCardRoot card) {
			StopTimer();
			BlockingPanelObject.SetActive(true);
			ChoicePanelObject.SetActive(true);
			_currentChoice = card;


			foreach (Transform child in GameObject.Find("ChoiceButtonPanel").transform) {
				child.gameObject.SetActive(true);
			}

			GameObject.Find("ChoiceButton1").GetComponent<Button>().interactable = true;
			GameObject.Find("ChoiceButton2").GetComponent<Button>().interactable = true;
			GameObject.Find("ChoiceButton3").GetComponent<Button>().interactable = true;


			SetChoicePanelOptions(card);
		}

		void MakeWeeklyDeductions() {
			_cash -= _weeklyExpense;
		}

		public void StartTimer() {
			_timerRunning = true;
		}

		public void StopTimer() {
			_timerRunning = false;
		}

		public void HideChoicePanel() {
			ChoicePanelObject.SetActive(false);
		}

		public void ShowResultsPanel(string results) {
			ResultsPanelObject.transform.FindChild("ResultText").gameObject.GetComponent<Text>().text = results;
			ResultsPanelObject.SetActive(true);
		}

		public void HideResultsPanel() {
			ResultsPanelObject.SetActive(false);
			BlockingPanelObject.SetActive(false);
			_currentChoice = null;
			StartScrolling();
			StartTimer();
		}

		public void StartScrolling() {
			_scrollController.StartScrolling();
		}

		public void ApplyChoice(int option) {
			string resultText;
			int cashToAdd;
			int weeklyPaymentToAdd;
			int satisfactionToAdd;

			switch (option) {
				case 1:
					resultText = _currentChoice.ResultDescription1;
					cashToAdd = _currentChoice.CostImmediate1;
					weeklyPaymentToAdd = _currentChoice.CostWeekly1;
					satisfactionToAdd = _currentChoice.SatisfactionEffect1;
					break;
				case 2:
					resultText = _currentChoice.ResultDescription2;
					cashToAdd = _currentChoice.CostImmediate2;
					weeklyPaymentToAdd = _currentChoice.CostWeekly2;
					satisfactionToAdd = _currentChoice.SatisfactionEffect2;
					break;
				default:
					resultText = _currentChoice.ResultDescription3;
					cashToAdd = _currentChoice.CostImmediate3;
					weeklyPaymentToAdd = _currentChoice.CostWeekly3;
					satisfactionToAdd = _currentChoice.SatisfactionEffect3;
					break;
			}
			ShowResultsPanel(resultText);

			AddCash(-cashToAdd);
			AddWeeklyExpense(-weeklyPaymentToAdd);
			AddSatisfaction(satisfactionToAdd);
		}

		public int GetCash() {
			return _cash;
		}

		public void AddCash(int cashToAdd) {
			_cash += cashToAdd;
		}

		public void AddPay() {
			_cash += Pay;
		}

		public int GetWeeklyExpense() {
			return _weeklyExpense;
		}

		public void AddWeeklyExpense(int expenseToAdd) {
			_weeklyExpense += expenseToAdd;
		}

		public void AddSatisfaction(int satToAdd) {
			Debug.Log("Added to Satisfaction: " + satToAdd);
			_satisfaction += satToAdd;
			_satisfactionSlider.value = _satisfaction;
		}

		private void CopyCardDetails(ThoughtCard cardData, GameObject cardroot) {
			cardroot.GetComponent<ThoughtCardController>().CardInfo = new ThoughtCardRoot();
			var temp = cardroot.GetComponent<ThoughtCardController>().CardInfo;
			temp.CardColor = cardData.CardColor;
			temp.Text = cardData.Text;
			temp.Description = cardData.Description;
			temp.OptionDescription1 = cardData.OptionDescription1;
			temp.OptionDescription2 = cardData.OptionDescription2;
			temp.OptionDescription3 = cardData.OptionDescription3;
			temp.ResultDescription1 = cardData.ResultDescription1;
			temp.ResultDescription2 = cardData.ResultDescription2;
			temp.ResultDescription3 = cardData.ResultDescription3;
			temp.CostImmediate1 = cardData.CostImmediate1;
			temp.CostImmediate2 = cardData.CostImmediate2;
			temp.CostImmediate3 = cardData.CostImmediate3;
			temp.CostWeekly1 = cardData.CostWeekly1;
			temp.CostWeekly2 = cardData.CostWeekly2;
			temp.CostWeekly3 = cardData.CostWeekly3;
			temp.SatisfactionEffect1 = cardData.SatisfactionEffect1;
			temp.SatisfactionEffect2 = cardData.SatisfactionEffect2;
			temp.SatisfactionEffect3 = cardData.SatisfactionEffect3;
		}

		private void SetChoicePanelOptions(ThoughtCardRoot card) {
			ChoicePanelObject.transform.FindChild("ChoiceText").GetComponent<Text>().text = card.Description;

			GameObject.Find("ChoiceButton1").transform.GetChild(0).GetComponent<Text>().text = card.OptionDescription1;
			if (_cash < card.CostImmediate1) {
				GameObject.Find("ChoiceButton1").GetComponent<Button>().interactable = false;
			}

			if (card.OptionDescription2.Length == 0) {
				GameObject.Find("ChoiceButton2").SetActive(false);
			}
			else {
				GameObject.Find("ChoiceButton2").transform.GetChild(0).GetComponent<Text>().text = card.OptionDescription2;
				if (_cash < card.CostImmediate2) {
					GameObject.Find("ChoiceButton2").GetComponent<Button>().interactable = false;
				}
			}

			if (card.OptionDescription3.Length == 0) {
				GameObject.Find("ChoiceButton3").SetActive(false);
			}
			else {
				GameObject.Find("ChoiceButton3").transform.GetChild(0).GetComponent<Text>().text = card.OptionDescription3;
				if (_cash < card.CostImmediate3) {
					GameObject.Find("ChoiceButton3").GetComponent<Button>().interactable = false;
				}
			}
		}
	}
}
