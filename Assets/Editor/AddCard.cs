﻿using Assets.Scripts;
using SeaMonster.Editor;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor {
	public class AddCard : MonoBehaviour {

		[MenuItem("Assets/Create/Moneyversity/New Card")]
		public static void CreateCard() {
			var card = ScriptableObjectUtility.CreateAsset<ThoughtCard>("Card");
			EditorUtility.SetDirty(card);
			AssetDatabase.SaveAssets();
		}

		[MenuItem("Assets/Create/Moneyversity/Add 10 Cards")]
		public static void Create10Cards() {
			for (int i = 0; i < 10; i++) {
				var card = ScriptableObjectUtility.CreateAsset<ThoughtCard>((i+1).ToString("D3"));
				card.Text = (i + 1).ToString("D3");
				EditorUtility.SetDirty(card);
				AssetDatabase.SaveAssets();
			}
		}
	}
}
